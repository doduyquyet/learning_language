package com.ava.albaladnews;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.ava.albaladnews.helper.ImageLoaderOptionProvider;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FullImageActivity extends Activity{

	public static final String URL = "url";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_full_image_view);
		
		Bundle extras = getIntent().getExtras();
		String url = (String) extras.get(URL);
		if(url == null) return;
		
		ImageView fullImageView 
				= (ImageView) findViewById(R.id.fullImage);
		
		ImageLoader.getInstance()
		.displayImage(url, fullImageView, 
				ImageLoaderOptionProvider.DEFAULT_OPTIONS);
	}
}
