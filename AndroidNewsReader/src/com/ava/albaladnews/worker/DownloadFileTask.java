package com.ava.albaladnews.worker;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.AsyncTask;

public class DownloadFileTask extends AsyncTask<String, Integer, String> {
	public static final String DOWLOADED = "Downloaded";
	protected String fileName;
	protected String savePath;
	protected ProgressListener progressListener;
	private FileOutputStream mOutputStream;
	
	public DownloadFileTask(String fileName, String savePath) {
		this.fileName = fileName;
		this.savePath = savePath;
	}
	
	public DownloadFileTask(FileOutputStream outputStream) {
		this.mOutputStream  = outputStream; 
	}
	
	public void setProgressListener(ProgressListener progressListener) {
		this.progressListener = progressListener;
	}

	@Override
	protected final String doInBackground(String... urls) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		HttpURLConnection connection = null;
		try {
			URL url = new URL(urls[0]);
			connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }
			
			int fileLength = connection.getContentLength();
			inputStream = connection.getInputStream();
            outputStream = (mOutputStream == null)?
            		new FileOutputStream(savePath + fileName) :
            			mOutputStream;
            
            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = inputStream.read(data)) != -1) {
                // allow canceling with back button
                if (isCancelled()) {
                    inputStream.close();
                    return null;
                }
                total += count;
                // publishing the progress....
                if (fileLength > 0) // only if total length is known
                    publishProgress((int) (total * 100 / fileLength));
                outputStream.write(data, 0, count);
            }
		} catch(Exception e) {
			return e.getMessage();
		} finally {
			try {
                if (outputStream != null)
                    outputStream.close();
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
		}
		
		return DOWLOADED;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if(hasListener()) {
			progressListener.onFinish(result);
		}
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		if(hasListener()) {
			progressListener.onProgressUpdate(values[0]);
		}
	}
	
	private boolean hasListener() {
		return progressListener != null;
	}
	
	public interface ProgressListener {
		void onFinish(String result);
		void onProgressUpdate(int percent);
	}
}

