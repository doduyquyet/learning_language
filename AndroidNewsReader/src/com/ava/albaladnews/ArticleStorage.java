package com.ava.albaladnews;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.ava.albaladnews.entities.Article;

public class ArticleStorage {
	private static final String FILE_NAME = "article.dat";
	private Context context;
	private List<Article> listArticles;

	public ArticleStorage(Context context) throws StreamCorruptedException, 
								ClassNotFoundException, IOException {
		this.context = context;
		loadArticles();
	}
	
	private void loadArticles() throws StreamCorruptedException, 
							IOException, ClassNotFoundException {
		FileInputStream inputStream = null;
		ObjectInputStream objectInputStream = null;
		try {
			inputStream = context.openFileInput(FILE_NAME);
			objectInputStream = new ObjectInputStream(inputStream);
			Object object = objectInputStream.readObject();
			
			listArticles = (List<Article>) object;
		} catch(FileNotFoundException e) {
			//ignore
		} finally {
			if(inputStream != null) {
				inputStream.close();
			}
			if(objectInputStream != null) {
				objectInputStream.close();
			}
			if(listArticles == null) {
				listArticles = new ArrayList<>();
			}
		}
	}

	public boolean add(Article article) {
		if(!listArticles.contains(article)) {
			listArticles.add(article);
			return true;
		}
		return false;
	}
	
	public boolean remove(Article article) {
		return listArticles.remove(article);
	}
	
	public void save() throws IOException {
		FileOutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		
		try {
			outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
			objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(listArticles);
		} finally {
			if(outputStream != null) {
				outputStream.close();
			}
			if(objectOutputStream != null) {
				objectOutputStream.close();
			}
		}
		
	}
	
	public void clear() {
		listArticles.clear();
	}
	
	public boolean contain(Article article) {
		return listArticles.contains(article);
	}
	
	public List<Article> getArticleList() {
		List<Article> clone = new ArrayList<>();
		for (Article article : listArticles) {
			clone.add((Article) article.clone());
		}
		return clone;
	}
	
	public boolean hasArticle() {
		return !listArticles.isEmpty();
	}
}
