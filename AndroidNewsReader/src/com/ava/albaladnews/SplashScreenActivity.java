package com.ava.albaladnews;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends Activity{
	private static final long DELAY = 2000;
	Timer timer;
	TimerTask task = new TimerTask() {
		@Override
		public void run() {
			runOnUiThread(runable);
		}
	};
	Runnable runable = new Runnable() {
		@Override
		public void run() {
			showMainActivity();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_splash_screen);
		timer = new Timer();
		timer.schedule(task, DELAY);
		super.onCreate(savedInstanceState);
	}

	protected void showMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
}
