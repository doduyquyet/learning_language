package com.ava.albaladnews.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.ava.albaladnews.entities.Article;

public class ArticleParser {
	private static final String LOG_TAG = ArticleParser.class.getSimpleName();
	private static final String NAME_SPACE = "";
	private static final String ROOT_TAG_NAME = "rss";
	private static final String ITEM_TAG_NAME = "item";
	private static final String TITLE_TAG_NAME = "title";
	private static final String DESCRIPTION_TAG_NAME = "description";
	private static final String LINK_TAG_NAME = "link";
	private static final String PUBLISH_DATE_TAG_NAME = "pubDate";
	private static final String IMG_URL_TAG_NAME = "enclosure";
	private static final String IGM_URL_ATTR_NAME = "url";
	
	private static ArticleParser instance = null;
	private final Map<String, Boolean> interestedTags = new HashMap<>();
	
	public static ArticleParser getInstance() {
		if(instance == null) {
			instance = new ArticleParser();
		}
		return instance;
	}
	
	private ArticleParser() {
		init();
	}
	
	private void init() {
		interestedTags.put(ROOT_TAG_NAME, true);
	}

	public List<Article> parse(InputStream input) throws ParseException {
		List<Article> returnList = null;
		try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(input, null);
            parser.nextTag();
            
            returnList = new ArrayList<>();
            parser.require(XmlPullParser.START_TAG, NAME_SPACE, ROOT_TAG_NAME);
            
            while(parser.next() != XmlPullParser.END_DOCUMENT) {
            	if(parser.getEventType() == XmlPullParser.END_TAG) {
            		continue;
            	}
            	
            	if(parser.getEventType() == XmlPullParser.START_TAG) {
            		String name = parser.getName();
                    switch(name) {
                    	case ITEM_TAG_NAME:
                    		returnList.add(getArticle(parser));
                		default:
                			continue;
                    }
                } 
            }
        } catch(Exception e) {
        	throw new ParseException("Parse articles failed!", e);
        }
		
		return returnList;
	}
	
	private Article getArticle(XmlPullParser parser) 
			throws XmlPullParserException, IOException {
		Article article = new Article();
		
		boolean isEnd = false;
		parser.next();
		while(!isEnd) {
			if(parser.getEventType() == XmlPullParser.END_DOCUMENT) {
				break;
			}
			
			if(parser.getEventType() == XmlPullParser.END_TAG) {
				if(parser.getName().equals(ITEM_TAG_NAME)) {
					parser.getName();
					break;
				}
				parser.next();
        		continue;
        	}
			
			if(parser.getEventType() == XmlPullParser.START_TAG) {
				String name = parser.getName();
				String text = parser.getText();
				
				if(name.equals(IMG_URL_TAG_NAME)) {
					String url = parser.getAttributeValue(null, IGM_URL_ATTR_NAME);
					setImgUrl(article, url);
				}
				
				int event = parser.next();
				if(event == XmlPullParser.TEXT) {
					text = parser.getText();
				} else if(event == XmlPullParser.END_DOCUMENT){
					break;
				} else {
					continue;
				}
				
				if(name.equals(TITLE_TAG_NAME)) {
					article.setTitle(text);
				} else if(name.equals(DESCRIPTION_TAG_NAME)) {
					article.setDescription(text);
				} else if(name.equals(LINK_TAG_NAME)) {
					article.setUrl(text);
				} else if(name.equals(PUBLISH_DATE_TAG_NAME)) {
					article.setPubDate(text);
				} 
			}
			parser.next();
		}
		
		return article;
	}


	private void setImgUrl(Article article, String url) {
		if(url.contains("/normal/")) {
			article.setFullImgUrl(url);
		} else {
			article.setImgUrl(url);
		}
	}


	public class ParseException extends Exception {
		private static final long serialVersionUID = 7391798787631172086L;

		public ParseException(String msg, Throwable e) {
			super(msg, e);
		}
	}
}
