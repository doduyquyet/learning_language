package com.ava.albaladnews.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.ava.albaladnews.R;
import com.ava.albaladnews.R.raw;
import com.ava.albaladnews.entities.Category;
import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.NSString;
import com.dd.plist.PropertyListParser;

import android.app.Activity;
import android.util.Log;

public class CategoryParser {
	private static final String TAG = CategoryParser.class.getSimpleName();
	private static final String PARSE_ERROR_MSG = "Parse category failed!";
	private static CategoryParser instance = null;
	public static CategoryParser getInstance() {
		if(instance == null) {
			instance = new CategoryParser();
		}
		return instance;
	}
	
	private CategoryParser() {
		
	}
	
	public List<Category> parse(InputStream is) throws ParseException {
		List<Category> listCategory = null;
		try {
		  NSArray root = (NSArray)PropertyListParser.parse(is);
		  listCategory = new ArrayList<>();
		  
		  NSObject[] parameters = (NSObject[]) root.getArray();
		  for(NSObject param : parameters) {
			NSDictionary mParam = (NSDictionary) param;
			NSObject name = mParam.get("name");
			NSObject url = mParam.get("url");
			Category category = new Category();
			
			if(name.getClass().equals(NSString.class)) {
			  NSString stringName = (NSString)name;
			  category.setName(stringName.toString());
		      Log.d(TAG, "Category name:" + stringName.toString());
			} else {
				Log.d(TAG, "Category not found");
			}
			
			if(url.getClass().equals(NSString.class)) {
				NSString stringUrl = (NSString)url;
				category.setUrl(stringUrl.toString());
				Log.d(TAG, "Category name:" + stringUrl.toString());
			} 
			
			listCategory.add(category);
		  }
		  
		} catch(Exception ex) {
			Log.e(TAG, PARSE_ERROR_MSG, ex);
			throw new ParseException(PARSE_ERROR_MSG, ex);
		}
		return listCategory;
	}
	
	public List<Category> parse(Activity context) throws ParseException {
		InputStream is = context.getResources().openRawResource(R.raw.categories);
		return parse(is);
	}
	
	public class ParseException extends Exception {
		private static final long serialVersionUID = 6290232352700742032L;

		public ParseException(String msg, Throwable e) {
			super(msg, e);
		}
	}
}
