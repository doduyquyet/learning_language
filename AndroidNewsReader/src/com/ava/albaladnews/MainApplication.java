package com.ava.albaladnews;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.acra.ReportingInteractionMode;

import android.app.Application;

@ReportsCrashes(
        formKey = "",
        formUri = "https://codart.cloudant.com/acra-app/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        formUriBasicAuthLogin="therwaindsomenessedgarth",
        formUriBasicAuthPassword="5tPYgPRepsgqAksjRM7CCsnw",
        // Your usual ACRA configuration
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_report_sent
        )
public class MainApplication extends Application{
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		ACRA.init(this);
	}
}
