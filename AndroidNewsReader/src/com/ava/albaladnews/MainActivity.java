package com.ava.albaladnews;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ava.albaladnews.entities.Article;
import com.ava.albaladnews.entities.Category;
import com.ava.albaladnews.fragment.AboutFragment;
import com.ava.albaladnews.fragment.ArticleListViewFragment;
import com.ava.albaladnews.fragment.FavoritesFragment;
import com.ava.albaladnews.fragment.SingleArticleFragment;
import com.ava.albaladnews.fragment.WebViewFragment;
import com.ava.albaladnews.helper.AwesomeIconMapper;
import com.ava.albaladnews.parser.CategoryParser;
import com.ava.albaladnews.view.FontAwesomeView;
import com.devsmart.android.ui.HorizontalListView;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class MainActivity extends ActionBarActivity {
	private static final String TAG = MainActivity.class.getSimpleName();
	private static final int MAX_BACK_PRESS_COMBO = 4; //mean press back button 5 time 
														//continustly will exit app
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView listViewCategory;
	private ListView listViewFixMenu;
	private List<Category> listCategory;
	private LinearLayout mLinearLayout;
	private ArticleStorage articleStorage;
	private HorizontalListView horizontalCategoryListView;
	private int activeCategoryIndex = 0;
	private TextView leftAction;
	private TextView rightAction;
	private List<Category> reversedData;
	private int backPressCombo = 0;
		

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "Activity create");
		loadArticleStorage();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		init();
		customizeActionBar();
		prepareImageLoader();
        prepareDrawer(savedInstanceState);
        prepareHorizontalCategoryMenu();
        
	}

	private void customizeActionBar() {
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.custom_actionbar);
		 
		final View customActionBarView = getSupportActionBar().getCustomView();
		leftAction = (TextView) customActionBarView.findViewById(R.id.left);
		rightAction = (TextView) customActionBarView.findViewById(R.id.right);
	}


	protected void onRightActionClick(View v) {
	}


	protected void onLeftActionClick(View v) {
	}


	private void prepareImageLoader() {
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(50 * 1024 * 1024) // 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.writeDebugLogs() // Remove for release app
				.build();
		ImageLoader.getInstance().init(config);
	}


	private void prepareHorizontalCategoryMenu() {
		horizontalCategoryListView
		.setAdapter(new HorizontalCategoryAdapter(reversedData));
		horizontalCategoryListView.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Category category = (Category)parent.getItemAtPosition(position);
				showCategory(category);
				activeCategoryIndex = position;
				HorizontalCategoryAdapter adapter
					= (HorizontalCategoryAdapter)horizontalCategoryListView.getAdapter();
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void loadArticleStorage() {
		try {
			articleStorage = new ArticleStorage(this);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	private void prepareDrawer(Bundle savedInstanceState) {
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        listViewCategory.setAdapter(new CategoryAdapter());
        listViewCategory.setOnItemClickListener(new CategoryItemClickListener());
        listViewFixMenu.setAdapter(new FixedListViewMenuAdapter());
        listViewFixMenu.setOnItemClickListener(new FixedMenuItemClickListener());
		
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        
        mDrawerToggle = new ActionBarDrawerToggle(this, 
        		mDrawerLayout, R.drawable.ic_drawer, 
        		R.string.menu_open, R.string.menu_close) {
        	@Override
        	public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(getTitle());
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        	@Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle("Menu");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        	@Override
			public boolean onOptionsItemSelected(MenuItem item) {
        		if (item != null && item.getItemId() == android.R.id.home) {
                    toogleDrawer();
                }
        		return false;
			}
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            showCategory(listCategory.get(0));
        }
		
	}
	private void prepareChangeFragment() {
		leftAction.setVisibility(View.VISIBLE);
		rightAction.setVisibility(View.VISIBLE);
		leftAction.setOnClickListener(null);
		rightAction.setOnClickListener(null);
		
		backPressCombo = 0;
	}
	
	public void showCategory(Category category) {
		prepareChangeFragment();
		ArticleListViewFragment fragment = ArticleListViewFragment.newInstance(category);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
		.addToBackStack(category.toString())
		.commit();
	}
	
	public void showFavoritesArticles() {
		prepareChangeFragment();
		FavoritesFragment fragment 
				= new FavoritesFragment();
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
		.addToBackStack(null)
		.commit();
	}
	

	public void showSingleArticle(Article article) {
		prepareChangeFragment();
		SingleArticleFragment fragment = SingleArticleFragment.newInstance(article);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
		.addToBackStack(article.toString())
		.commit();
	}
	
	public void showWebView(String title ,String url) {
		prepareChangeFragment();
		WebViewFragment fragment = WebViewFragment.newInstance(title, url);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
		.addToBackStack(url)
		.commit();
	}
	
	public void showAbout() {
		prepareChangeFragment();
		AboutFragment fragment = new AboutFragment();
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.replace(R.id.content_frame, fragment)
		.commit();
	}
	
	public ArticleStorage getArticleStorage() {
		return this.articleStorage;
	}
	
	public void setLeftActionIcon(int resId) {
		if(resId != 0) {
			String icon = getResources().getString(resId);
			leftAction.setText(icon);
		}
	}
	
	public void setRightActionIcon(int resId) {
		if(resId != 0) {
			String icon = getResources().getString(resId);
			rightAction.setText(icon);
		}
	}
	
	public TextView getLeftAction() {
		return leftAction;
	}
	
	public TextView getRightAction() {
		return rightAction;
	}
	
	public void showHorizontalMenu() {
		horizontalCategoryListView.setVisibility(View.VISIBLE);
	}
	public void hideHorizontalMenu() {
		horizontalCategoryListView.setVisibility(View.GONE);
	}
	
	public void showFullImageActivity(String url) {
		Intent intent = new Intent(this,  FullImageActivity.class);
		intent.putExtra(FullImageActivity.URL, url);
		startActivity(intent);
	}

	private void init() {
		try {
			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			listViewCategory = (ListView) findViewById(R.id.category_drawer);
			listViewFixMenu = (ListView) findViewById(R.id.fix_menu_drawer);
			mLinearLayout = (LinearLayout) findViewById(R.id.wrapper_layout);
			listCategory = CategoryParser.getInstance().parse(this);
			reversedData = CategoryParser.getInstance().parse(this);
			Collections.reverse(reversedData);
			horizontalCategoryListView = 
					(HorizontalListView) findViewById(R.id.horizontal_category_menu);
			
		} catch (CategoryParser.ParseException e) {
			Toast.makeText(getApplicationContext(), 
					e.getMessage(), Toast.LENGTH_LONG).show();;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "Option clicked!");
		if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
		return super.onOptionsItemSelected(item);
	}
	
	 @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mLinearLayout);
        return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public void onBackPressed() {
    	int currentFragCount = getSupportFragmentManager().getBackStackEntryCount();
        if (currentFragCount > 0 && backPressCombo < MAX_BACK_PRESS_COMBO){
            getSupportFragmentManager().popBackStack();
            backPressCombo++;
        } else {
        	super.onBackPressed();
        }
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	try {
			articleStorage.save();
		} catch (IOException e) {
			Log.e(TAG, e.getMessage(), e);
			Toast.makeText(this, getResources().getString(R.string.article_save_failed)
					, Toast.LENGTH_LONG).show();
		}
    }
    
//    /*-----------------------------------------------*/    
//    /*Classes----------------------------------------*/    
//    /*-----------------------------------------------*/    
//    
    private class FixedMenuItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			listViewCategory.setItemChecked(-1, true);
			
			if(position == FixedListViewMenuAdapter.HOME) {
				showCategory(listCategory.get(0));
			} else if(position == FixedListViewMenuAdapter.ABOUT) {
				showAbout();
			} else if(position == FixedListViewMenuAdapter.FAVORITES) {
				showFavorites();
			}
			
			mDrawerLayout.closeDrawer(mLinearLayout);
		}
    }
    
    private class CategoryItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Category category = (Category)parent.getItemAtPosition(position);
			showCategory(category);
			listViewFixMenu.setItemChecked(-1, true);
			mDrawerLayout.closeDrawer(mLinearLayout);
		}
    }
    
    private class FixedListViewMenuAdapter extends BaseAdapter {
    	public static final int HOME = 0;
    	public static final int ABOUT = 1;
    	public static final int FAVORITES = 2;
    	
    	private final String[] name = new String[] 
    			{getResources().getString(R.string.home) ,
    			getResources().getString(R.string.about), 
    			getResources().getString(R.string.favorites)};
    	private final String[] icon = new String[] 
    			{getResources().getString(R.string.fa_home),
    			getResources().getString(R.string.fa_question), 
				getResources().getString(R.string.fa_heart)};
    	
		@Override
		public int getCount() {
			return name.length;
		}

		@Override
		public Object getItem(int position) {
			return name[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
		    View row;
    		if(convertView == null) {
    			row = inflater.inflate(R.layout.
	    				drawer_menu_item, 
	    				parent, false);
    		} else {
    			row = convertView;
    		}
    		
    		TextView mTextView = (TextView) row.findViewById(R.id.text_view);
		    FontAwesomeView fontAwesomeView 
		    		= (FontAwesomeView) row.findViewById(R.id.icon);
		    
		    mTextView.setText(name[position]);
	    	fontAwesomeView.setText(icon[position]);
			
			return row;
		}
    	
    }
    
    private class HorizontalCategoryAdapter extends ArrayAdapter<Category> {

		public HorizontalCategoryAdapter(List<Category> data) {
			super(MainActivity.this, 
					R.layout.horizontal_listview_category_menu_item, data);
		}
    	
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
		    View item;
    		if(convertView == null) {
    			item = inflater.inflate(R.layout.
	    				horizontal_listview_category_menu_item, 
	    				parent, false);
    		} else {
    			item = convertView;
    		}
		    		
    		TextView view = (TextView) item.findViewById(R.id.item);
    		view.setText(this.getItem(position).getName());
    		
    		if(position == activeCategoryIndex) {
    			view.setBackgroundColor(getResources().getColor(R.color.semi_white));
    			view.setTextColor(getResources().getColor(R.color.base_theme_color));
    		} else {
    			restoreOriginalState(view);
    		}
    		
			return item;
		}
		
		private void restoreOriginalState(TextView view) {
			view.setBackgroundColor(getResources().getColor(R.color.red));
			view.setTextColor(getResources().getColor(R.color.semi_white));
		}
    }
    
    private class CategoryAdapter extends ArrayAdapter<Category> {

    	public CategoryAdapter() {
			super(MainActivity.this, R.layout.drawer_menu_item, listCategory);
		}
    	
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
		    View row = (convertView == null)?
		    		inflater.inflate(R.layout.drawer_menu_item, parent, false) :
		    		convertView;
    		Category category = this.getItem(position);
    		AwesomeIconMapper iconMapper = AwesomeIconMapper.getInstance();
    		Integer iconCode = iconMapper.getIconCode(category.getName());
		    TextView mTextView = (TextView) row.findViewById(R.id.text_view);
		    FontAwesomeView fontAwesomeView 
		    		= (FontAwesomeView) row.findViewById(R.id.icon);
		    
		    mTextView.setText(category.toString());
		    if(iconCode != null) {
		    	fontAwesomeView.setText(getResources().getText(iconCode));
		    }
		    
			return row;
		}
		
		@Override
		public int getCount() {
			return listCategory.size() - 1;
		}
		
		@Override
		public Category getItem(int position) {
			return listCategory.get(position + 1);
		}
		
		@Override
		public long getItemId(int position) {
			return listCategory.get(position + 1).hashCode();
		}
    }
	public void toogleDrawer() {
		 if (mDrawerLayout.isDrawerOpen(mLinearLayout)) {
	         mDrawerLayout.closeDrawer(mLinearLayout);
	     } else {
	         mDrawerLayout.openDrawer(mLinearLayout);
	     }
	}


	public void showFavorites() {
		showFavoritesArticles();
	}


	
    
}
