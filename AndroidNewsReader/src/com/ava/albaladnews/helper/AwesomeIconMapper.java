package com.ava.albaladnews.helper;

import java.util.HashMap;
import java.util.Map;

import com.ava.albaladnews.R;

public class AwesomeIconMapper {
	private static AwesomeIconMapper instance = null;
	private Map<String, Integer> map;
	
	public static AwesomeIconMapper getInstance() {
		if(instance == null) {
			instance = new AwesomeIconMapper();
		}
		return instance;
	}
	
	private AwesomeIconMapper() {
		map = new HashMap<>();
		setUp();
	}
	
	private void setUp() {
		map.put("مقالات و كتب", R.string.fa_book);
		map.put("مال و إقتصاد", R.string.fa_dollar);
		map.put("عربى و عالمى", R.string.fa_globe);
		map.put("محليات", R.string.fa_map_marker);
//		map.put("", R.string.fa_users);
		map.put("الرئيسية", R.string.fa_home);
//		map.put("", R.string.fa_question);
		map.put("أخبار الرياضة", R.string.fa_soccer_ball_o);
		map.put("منوعات", R.string.fa_soccer_ball_o);
	}

	public Integer getIconCode(String key) {
		Integer r = map.get(key);
		return r;
	}
	
	public boolean hasKey(String key) {
		return map.containsKey(key);
	}
}
