package com.ava.albaladnews.helper;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ImageLoaderOptionProvider {
	public static final DisplayImageOptions 
		DEFAULT_OPTIONS = new DisplayImageOptions.Builder()
						.cacheInMemory(true)
						.cacheOnDisk(true)
						.considerExifParams(true)
						.build();
	
	public static final DisplayImageOptions 
		THUMBAIL_OPTIONS = new DisplayImageOptions.Builder()
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.displayer(new RoundedBitmapDisplayer(20))
		.build();
}
