package com.ava.albaladnews.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontAwesomeView extends TextView {
	private static final String FONT_PATH = "fonts/fontawesome-webfont.ttf";
	private static Typeface awesomeType = null;

    public FontAwesomeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public FontAwesomeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FontAwesomeView(Context context) {
        super(context);
        init();
    }

    private void init() {
    	if(awesomeType == null) {
    		awesomeType = Typeface.createFromAsset(
    				getContext().getAssets(), FONT_PATH);
    	}
        setTypeface(awesomeType);
    }

}
