package com.ava.albaladnews.view;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class VerticalViewPager extends ViewPager {

	private Timer timer = null;
	private int periodInSecond = 2;
	private Activity activity = null;
	private int current = 0;

	public VerticalViewPager(Context context) {
	    super(context);
	    init();
	}
	
	public VerticalViewPager(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    init();
	}
	
	private void init() {
	    setPageTransformer(true, new VerticalPageTransformer());
	    setOverScrollMode(OVER_SCROLL_NEVER);
	}
	
	private class VerticalPageTransformer implements ViewPager.PageTransformer {
	
	    @Override
	    public void transformPage(View view, float position) {
	        int pageWidth = view.getWidth();
	        int pageHeight = view.getHeight();
	
	        if (position < -1) { 
	            view.setAlpha(0);
	
	        } else if (position <= 1) { // [-1,1]
	            view.setAlpha(1);
	
	            view.setTranslationX(pageWidth * -position);
	
	            float yPosition = position * pageHeight;
	            view.setTranslationY(yPosition);
	
	        } else { 
	            view.setAlpha(0);
	        }
	    }
	}
	
	public void setUp(PagerAdapter adapter, Activity activity) {
		setAdapter(adapter);
		this.activity  = activity;
	}
	
	@Override
	public void setAdapter(PagerAdapter adapter) {
		super.setAdapter(adapter);
		initilizeTimer();
	}
	
	private void initilizeTimer() {
		timer = new Timer();
		timer.scheduleAtFixedRate(new SliderTask(), 0, periodInSecond * 1000);
	}

	/**
	 * Swaps the X and Y coordinates of your touch event
	 */
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
//	    ev.setLocation(ev.getY(), ev.getX());
	    return false;
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		return false;
	}
	
	public void destroy() {
		if(timer != null) {
			timer.cancel();
		}
	}
	
	private class SliderTask extends TimerTask {
		@Override
		public void run() {
			boolean preCondition = (activity != null);
			if(!preCondition) {
				destroy();
				return;
			}
			//else do the task:
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(current < getAdapter().getCount()) {
						setCurrentItem(current++);
					} else {
						current = 0;
						setCurrentItem(current);
					}
				}
			});
		}
	}
}
