package com.ava.albaladnews.fragment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;
import com.ava.albaladnews.entities.Article;
import com.ava.albaladnews.entities.Category;
import com.ava.albaladnews.parser.ArticleParser;
import com.ava.albaladnews.parser.ArticleParser.ParseException;
import com.ava.albaladnews.view.VerticalViewPager;
import com.ava.albaladnews.worker.DownloadFileTask;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

public class ArticleListViewFragment extends Fragment {
	
	public static final String CATEGORY_KEY = "category_key";
	public static final String TMP_FILE_NAME = "tmp.xml";
	private static final String TAG = ArticleListViewFragment.class.getSimpleName();
	private static final String LIST_ARTICLE_KEY = null;
	private Category category;
	private ListView listViewArticle;
	private View rootView;
	private VerticalViewPager viewPager;
	private List<Article> listArticles;
	private ProgressBar spinner;
	private MainActivity mainActivity;
	private DownloadFileTask.ProgressListener listener
	= new DownloadFileTask.ProgressListener() {
		@Override
		public void onProgressUpdate(int percent) {
			
		}
		@Override
		public void onFinish(String result) {
			if(ArticleListViewFragment.this.getActivity() == null)
				return;
			
			Toast.makeText(ArticleListViewFragment.this.getActivity(), 
					result, Toast.LENGTH_LONG).show();
			if(result != null && result.equals(DownloadFileTask.DOWLOADED)) {
				showArticle();
			}
		}
	};
	private DisplayImageOptions options;
	private ImageLoader imageLoader;
	

	public static ArticleListViewFragment newInstance(Category category) {
		ArticleListViewFragment instance = new ArticleListViewFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(CATEGORY_KEY, category);
		instance.setArguments(bundle);
		return instance;
	}
	
	public static ArticleListViewFragment newInstance(List<Article> listArticle) {
		ArticleListViewFragment instance = new ArticleListViewFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(LIST_ARTICLE_KEY, new ArrayList<>(listArticle));
		instance.setArguments(bundle);
		return instance;
	}
	
	public ArticleListViewFragment() {
	}
	
	private void init() {
		
		options = new DisplayImageOptions.Builder()
			.postProcessor(new BitmapProcessor() {
				@Override
				public Bitmap process(Bitmap bitmap) {
					Bitmap dstBmp;
					if (bitmap.getWidth() >= bitmap.getHeight()){

					  dstBmp = Bitmap.createBitmap(
						 bitmap, 
						 bitmap.getWidth()/2 - bitmap.getHeight()/2,
					     0,
					     bitmap.getHeight(), 
					     bitmap.getHeight()
					     );

					}else{

					  dstBmp = Bitmap.createBitmap(
						 bitmap,
					     0, 
					     bitmap.getHeight()/2 - bitmap.getWidth()/2,
					     bitmap.getWidth(),
					     bitmap.getWidth() 
					     );
					}
					return dstBmp;
				}
			})
//			.showImageOnLoading(R.drawable.ic_stub)
//			.showImageForEmptyUri(R.drawable.ic_empty)
//			.showImageOnFail(R.drawable.ic_error)
			.cacheInMemory(true)
			.cacheOnDisk(true)
			.considerExifParams(true)
			.displayer(new RoundedBitmapDisplayer(20))
			.build();
		
		imageLoader = ImageLoader.getInstance();
		
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mainActivity = (MainActivity) activity;
	}
	
		
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
		setHasOptionsMenu(true);
	}
	
	private void showArticle() {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = getActivity().openFileInput(TMP_FILE_NAME);
			listArticles 
				= ArticleParser.getInstance().parse(fileInputStream);
			
			listViewArticle.setAdapter(new ArticleAdapter(listArticles));
			viewPager.setUp(new MyPagerAdapter(), getActivity());
			spinner.setVisibility(View.GONE);
			viewPager.setVisibility(View.VISIBLE);
			listViewArticle.setVisibility(View.VISIBLE);
			
		} catch(ParseException | IOException e) {
			Toast.makeText(getActivity(), 
					e.getMessage(), Toast.LENGTH_LONG).show();
		} finally {
			try {
				if(fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException ignore) {}
		}
	}
	
	private void showArticleWithExistedList(List<Article> listArticles) {
		ProgressBar spinner = (ProgressBar) rootView.findViewById(R.id.spinner);
		listViewArticle.setAdapter(new ArticleAdapter(listArticles));
		spinner.setVisibility(View.GONE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container,
			Bundle savedInstanceState) {
		category = (Category) getArguments().getSerializable(CATEGORY_KEY);
		rootView = inflater.inflate(R.layout.frag_article_list, container, false);
		viewPager = (VerticalViewPager) rootView.findViewById(R.id.pager);
		listViewArticle = (ListView) rootView.findViewById(R.id.list_article);
		spinner = (ProgressBar) rootView.findViewById(R.id.spinner);
		listViewArticle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				MainActivity mainActivity = (MainActivity) getActivity();
				mainActivity.showSingleArticle(
						(Article )parent.getItemAtPosition(position));
			}
		});
		
		List<Article> retrivedList = 
				(List<Article>) getArguments().getSerializable(LIST_ARTICLE_KEY);
		if(retrivedList != null) {
			showArticleWithExistedList(retrivedList);
		} else {
			downloadFeed();
		}
		
		setUpParentBehaviour();
		
		return rootView;
	}
	
	private void setUpParentBehaviour() {
		mainActivity.showHorizontalMenu();
		mainActivity.setLeftActionIcon(R.string.fa_rotate_left);
		mainActivity.setRightActionIcon(R.string.fa_bars);
		mainActivity.getLeftAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				refresh();
			}
		});
		mainActivity.getRightAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mainActivity.toogleDrawer();
			}
		});
	}

	private void downloadFeed() {
		DownloadFileTask task;
		try {
			task = new DownloadFileTask(
				getActivity().openFileOutput(TMP_FILE_NAME, Context.MODE_PRIVATE));
		
			task.execute(category.getUrl());
			task.setProgressListener(listener);
			
		} catch (FileNotFoundException e) {
			Log.e(TAG, e.getMessage(), e);
			Toast.makeText(getActivity(), 
					e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}

	
	private void refresh() {
		spinner.setVisibility(View.VISIBLE);
		viewPager.setVisibility(View.GONE);
		listViewArticle.setVisibility(View.GONE);
		downloadFeed();
	}
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.article_list, menu);
	}
	
	
	/*------------------------------Class-------------------------------*/	
	private class ArticleAdapter extends ArrayAdapter<Article> {
		public ArticleAdapter(List<Article> data) {
			super(getActivity(), R.layout.article_item, data);
		}
		
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View row = (convertView == null)?
					inflater.inflate(R.layout.article_item, parent, false) 
					:convertView;
					Article article = this.getItem(position);
					TextView textViewTitle = (TextView) row.findViewById(R.id.title);
					TextView textViewPubDate = (TextView) row.findViewById(R.id.pubDate);
					ImageView thum = (ImageView) row.findViewById(R.id.thumb_image);
					
					textViewTitle.setText(article.getTitle());
					textViewPubDate.setText(article.getPubDate());
					imageLoader.displayImage(article.getImgUrl(), thum, options);
					
					return row;
		}
		
	}

	private class MyPagerAdapter extends FragmentStatePagerAdapter {

		public MyPagerAdapter() {
			super(getActivity().getSupportFragmentManager());
		}

		@Override
		public Fragment getItem(int arg0) {
			return PagerItemFragment.newInstance(listArticles.get(arg0));
		}

		@Override
		public int getCount() {
			return listArticles.size();
		}
		
	}



}
