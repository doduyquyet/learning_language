package com.ava.albaladnews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;

public class WebViewFragment extends Fragment {
	private static final String URL_KEY = "url";
	private static final String TITLE_KEY = "title";

	public static WebViewFragment newInstance(String title, String url) {
		WebViewFragment instance = new WebViewFragment();
		Bundle bundle = new Bundle();
		bundle.putString(URL_KEY, url);
		bundle.putString(TITLE_KEY, title);
		instance.setArguments(bundle);
		return instance;
	}
	
	public WebViewFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.frag_web_view, container, false);
		WebView webView = (WebView) rootView.findViewById(R.id.web_view);
		
		webView.loadUrl(getArguments().getString(URL_KEY));
		
		setUpParentBehaviour((MainActivity) getActivity());
		
		return rootView;
	}

	private void setUpParentBehaviour(final MainActivity activity) {
		activity.setLeftActionIcon(R.string.fa_share_alt);
		activity.setRightActionIcon(R.string.fa_arrow_circle_o_right);
		activity.getLeftAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				share();
			}
		});
		activity.getRightAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.onBackPressed();
			}
		});
	}

	private void share() {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_SUBJECT, getArguments().getString(TITLE_KEY));
		i.putExtra(Intent.EXTRA_TEXT, getArguments().getString(URL_KEY));
		startActivity(
				Intent.createChooser(i, getResources().getString(R.string.share)) );
	}

}
