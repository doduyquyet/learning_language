package com.ava.albaladnews.fragment;

import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AboutFragment extends Fragment {
	private View rootView;

	public AboutFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.frag_about, container, false);
		
		View viewEmail = rootView.findViewById(R.id.email);
		View viewPhone = rootView.findViewById(R.id.phone);
		View viewTwitter = rootView.findViewById(R.id.twitter);
		
		viewEmail.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendEmail(getString(R.string.about_email));
			}
		});
		viewPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				call();
			}
		});
		viewTwitter.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				visitUrl(getString(R.string.about_twitter_url));
			}
		});
		
		setUpParentBehaviour((MainActivity) getActivity());
		
		return rootView;
	}

	protected void sendEmail(String url) {
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
	            "mailto", url, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
		startActivity(Intent.createChooser(emailIntent, "Send email..."));
	}
	
	public void call() {   
        Intent callIntent = new Intent(Intent.ACTION_CALL);          
        callIntent.setData(Uri.parse("tel:" + getResources()
        		.getInteger(R.integer.phone_num)));          
        startActivity(callIntent);  
	}
	
	public void visitUrl(String url) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(browserIntent);
	}

	private void setUpParentBehaviour(final MainActivity activity) {
		activity.hideHorizontalMenu();
		activity.getLeftAction().setVisibility(View.INVISIBLE);
		activity.setRightActionIcon(R.string.fa_bars);
		activity.getRightAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.toogleDrawer();
			}
		});
	}


}
