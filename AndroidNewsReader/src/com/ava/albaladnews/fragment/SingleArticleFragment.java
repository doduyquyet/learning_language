package com.ava.albaladnews.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;
import com.ava.albaladnews.entities.Article;
import com.loopj.android.image.SmartImageView;

public class SingleArticleFragment extends Fragment {
	
	private static final String ARTICLE_KEY = "article";
	private Article article;
	
	public static SingleArticleFragment newInstance(Article article) {
		SingleArticleFragment instance = new SingleArticleFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ARTICLE_KEY, article);
		instance.setArguments(bundle);
		
		return instance;
	}
	
	public SingleArticleFragment() {
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		article = (Article) getArguments().getSerializable(ARTICLE_KEY);
		View rootView = inflater.inflate(R.layout.frag_single_article, container, false);
		TextView textViewPubDate = (TextView) rootView.findViewById(R.id.pubDate);
		TextView textViewTitle = (TextView) rootView.findViewById(R.id.title);
		SmartImageView imageViewMain = (SmartImageView) rootView.findViewById(R.id.main_img);
		TextView textViewDescription = (TextView) rootView.findViewById(R.id.description);
		Button btnOpenUrl = (Button) rootView.findViewById(R.id.btn_open_url);
		Button btnOpenFullImage = (Button) rootView.findViewById(R.id.btn_view_full_img);
		
		textViewPubDate.setText(article.getPubDate());
		textViewTitle.setText(article.getTitle());
		imageViewMain.setImageUrl(article.getFullImgUrl());
		textViewDescription.setText( Html.fromHtml(article.getDescription()));
		btnOpenUrl.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) getActivity())
					.showWebView(article.getTitle(), article.getUrl());
			}
		});
		btnOpenFullImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) getActivity())
					.showFullImageActivity(article.getFullImgUrl());
			}
		});
		
		setUpParentBeahaviour((MainActivity) getActivity());
		
		return rootView;
	}

	private void setUpParentBeahaviour(final MainActivity activity) {
		activity.setLeftActionIcon(R.string.fa_star);
		activity.setRightActionIcon(R.string.fa_arrow_circle_o_right);
		activity.hideHorizontalMenu();
		activity.getLeftAction()
			.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onFavoritesClick(activity);
			}
		});
		activity.getRightAction()
			.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.onBackPressed();
			}
		});
	}

	protected void onFavoritesClick(MainActivity activity) {
		if(!activity.getArticleStorage().contain(article)) {
			activity.getArticleStorage().add(article);
			Toast.makeText(activity, 
					getResources().getText(R.string.article_added), 
					Toast.LENGTH_SHORT).show();
			return;
		}
		Toast.makeText(activity, 
				getResources().getText(R.string.article_already_added), 
				Toast.LENGTH_SHORT).show();
	}
	
}
