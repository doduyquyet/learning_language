package com.ava.albaladnews.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;
import com.ava.albaladnews.entities.Article;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class PagerItemFragment extends Fragment{
	private static final String KEY = "article";
	private View rootView;
	private ImageView imageView;
	private DisplayImageOptions options;
	private ImageLoader imageLoader;
	private TextView textViewTitle;
	private Article article;
	private OnClickListener listener;
	private TextView textViewPubDate;

	public static PagerItemFragment newInstance(Article article) {
		PagerItemFragment instance = new PagerItemFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(KEY, article);
		instance.setArguments(bundle);
		return instance;
	}
	
	public PagerItemFragment() {
		init();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		listener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				showArticle();
			}
		};
	}
	
	protected void showArticle() {
		((MainActivity) getActivity()).showSingleArticle(article);
	}

	private void init() {
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.build();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		article = (Article) getArguments().getSerializable(KEY);
		rootView = inflater.inflate(R.layout.frag_pager_item, container, false);
		imageView = (ImageView) rootView.findViewById(R.id.previewImg);
		textViewTitle = (TextView) rootView.findViewById(R.id.title);
		textViewPubDate = (TextView) rootView.findViewById(R.id.pubDate);
		
		imageLoader.displayImage(article.getFullImgUrl(), imageView, options);
		textViewTitle.setText(article.getTitle());
		textViewPubDate.setText(article.getPubDate());
		rootView.setOnClickListener(listener);
		
		return rootView;
	}
}
