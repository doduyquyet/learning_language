package com.ava.albaladnews.fragment;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ava.albaladnews.ArticleStorage;
import com.ava.albaladnews.MainActivity;
import com.ava.albaladnews.R;
import com.ava.albaladnews.entities.Article;
import com.ava.albaladnews.helper.ImageLoaderOptionProvider;
import com.nostra13.universalimageloader.core.ImageLoader;

public class FavoritesFragment extends Fragment{
	private View rootView;
	private ListView listViewArticle;
	private MainActivity mainActivity;
	private ArticleStorage storage;
	private boolean editMode = false;
	private ArticleAdapter adapter;
	private List<Article> lisArticles;

	public FavoritesFragment() {
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainActivity = (MainActivity) getActivity();
		storage = (ArticleStorage) mainActivity.getArticleStorage();
		rootView = inflater.inflate(R.layout.frag_favorites, container, false);
		listViewArticle = (ListView) rootView.findViewById(R.id.list_article);
		
		lisArticles = storage.getArticleList();
		adapter = new ArticleAdapter(lisArticles);
		listViewArticle.setAdapter(adapter);
		listViewArticle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(editMode) return;
				MainActivity mainActivity = (MainActivity) getActivity();
				mainActivity.showSingleArticle(
						(Article )parent.getItemAtPosition(position));
			}
		});
		
		setUpParentBeahaviour((MainActivity) getActivity());
		
		return rootView;
	}
	
	private void setUpParentBeahaviour(final MainActivity activity) {
		activity.setLeftActionIcon(R.string.fa_edit);
		activity.setRightActionIcon(R.string.fa_bars);
		
		activity.getRightAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				activity.toogleDrawer();
			}
		});
		
		activity.getLeftAction()
		.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toogleEditMode();
			}
		});
		
		activity.hideHorizontalMenu();
	}

	protected void toogleEditMode() {
		editMode = editMode? false : true ;
		adapter.notifyDataSetChanged();
	}

	private class ArticleAdapter extends ArrayAdapter<Article> {
		public ArticleAdapter(List<Article> data) {
			super(getActivity(), R.layout.favorites_item, data);
		}
		
		
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View row = (convertView == null)?
					inflater.inflate(R.layout.favorites_item, parent, false) 
					:convertView;
			final Article article = this.getItem(position);
			TextView textViewTitle = (TextView) row.findViewById(R.id.title);
			TextView textViewPubDate = (TextView) row.findViewById(R.id.pubDate);
			ImageView thum = (ImageView) row.findViewById(R.id.thumb_image);
			TextView delete = (TextView) row.findViewById(R.id.delete);
			View overlay = (View) row.findViewById(R.id.overlay);
			
			textViewTitle.setText(article.getTitle());
			textViewPubDate.setText(article.getPubDate());
			ImageLoader.getInstance()
				.displayImage(article.getImgUrl(), thum, 
						ImageLoaderOptionProvider.THUMBAIL_OPTIONS);
			
			if(editMode) {
				overlay.setVisibility(View.VISIBLE);
			} else {
				overlay.setVisibility(View.GONE);
			}
			
			delete.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					storage.remove(article);
					lisArticles.remove(article);
					notifyDataSetChanged();
				}
			});
			
			return row;
		}


		
	}
}
