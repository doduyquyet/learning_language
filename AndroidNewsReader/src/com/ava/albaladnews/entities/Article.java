package com.ava.albaladnews.entities;

import java.io.Serializable;

public class Article implements Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -644883207482342436L;
	private Category category;
	private String title;
	private String imgUrl;
	private String fullImgUrl;
	private String url;
	private String description;
	private String pubDate;
	
	public Article() {
	}
	
	public Article(Category category, String title, String imgUrl, String fullImgUrl,
			String description, String url, String pubDate) {
		this.category = category;
		this.title = title;
		this.imgUrl = imgUrl;
		this.fullImgUrl = fullImgUrl;
		this.description = description;
		this.url = url;
		this.pubDate = pubDate;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	public String getFullImgUrl() {
		return fullImgUrl;
	}
	
	public void setFullImgUrl(String fullImgUrl) {
		this.fullImgUrl = fullImgUrl;
	}
	
	@Override
	public boolean equals(Object o) {
		Article other;
		other = (Article) o;
		return this.url.equals(other.url);
	}
	
	@Override
	public int hashCode() {
		return this.url.hashCode();
	}
	
	@Override
	public String toString() {
		return this.title;
	}
	
	@Override
	public Object clone(){
		Article clone = new Article();
		clone.description = this.description ;
		clone.imgUrl = this.imgUrl;
		clone.fullImgUrl = this.fullImgUrl;
		clone.pubDate = this.pubDate;
		clone.title = this.title;
		clone.url = this.url;
		
		return clone;
	}

}
